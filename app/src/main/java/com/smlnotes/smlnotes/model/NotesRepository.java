package com.smlnotes.smlnotes.model;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;

import com.smlnotes.lib.AbstractObjectStore;
import com.smlnotes.smlnotes.common.AppExecutors;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.model.database.NoteDao;
import com.smlnotes.smlnotes.model.network.SyncService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotesRepository extends AbstractObjectStore<Note> {

    public static final String LAST_SYNC_KEY = "key_last_sync";
    public static final String CLIENT_ID_KEY = "key_client_id";
    private final NoteDao noteDao;
    private final AppExecutors appExecutors;
    private final SharedPreferences sharedPreferences;
    private final Context context;

    private long lastSyncTime = 0;

    public NotesRepository(Context context, NoteDao noteDao, AppExecutors appExecutors, SharedPreferences sharedPreferences) {
        this.context = context;
        this.noteDao = noteDao;
        this.appExecutors = appExecutors;
        this.sharedPreferences = sharedPreferences;
    }

    public LiveData<List<Note>> getNotes() {
        return noteDao.loadAll();
    }

    public LiveData<List<Note>> getNotDeletedNotes() {
        return noteDao.loadAllNotDeleted();
    }

    public LiveData<List<Note>> getNotDeletedNotesMatching(String searchTerm) {
        return noteDao.loadAllNotDeletedMatching(searchTerm);
    }

    public LiveData<Note> getNote(int id) {
        return noteDao.load(id);
    }

    public void syncNotes(boolean force) {
        if (!force && System.currentTimeMillis() - lastSyncTime < 5000)
            return;
        lastSyncTime = System.currentTimeMillis();
        SyncService.sync(context);
    }

    public void clearSyncedNotes() {
        appExecutors.diskIO().execute(noteDao::clearSyncedNotes);
    }

    public void updateNote(Note note) {
        appExecutors.diskIO().execute(() -> {
            if (note.getContent().trim().isEmpty()) {
                if (note.isLocal()) {
                    noteDao.delete(note.getId());
                } else {
                    deleteElement(String.valueOf(note.getID()));
                }
            } else {
                updateElement(String.valueOf(note.getID()), note.getContent(), Calendar.getInstance());
            }
        });
    }

    @Override
    public List<Note> getChangedElements(Calendar lastSynced) {
        return noteDao.loadAllAfter(lastSynced.getTimeInMillis());
    }

    @Override
    public List<String> getDeletedElements() {
        List<Integer> deleted = noteDao.getDeletedIds();
        List<String> ids = new ArrayList<>();
        if (deleted != null) {
            for (Integer i : deleted) {
                ids.add(i.toString());
            }
        }
        return ids;
    }

    @Override
    public void onElementSynced(String id, boolean deleted) {
        if (deleted) {
            appExecutors.diskIO().execute(() -> noteDao.delete(Integer.valueOf(id)));
        } else {
            appExecutors.diskIO().execute(() -> noteDao.flagNotLocal(Integer.valueOf(id)));
        }
    }

    @Override
    public Note createElement(String content, Calendar creationTime) {
        Note note = new Note(creationTime.getTimeInMillis(), content, false, false);
        long id = noteDao.insert(note);
        note.setId((int) id);
        return note;
    }

    public void createElementLocal(OnNoteCreatedListener listener) {
        Note note = new Note(Calendar.getInstance().getTimeInMillis(), "", true, false);
        appExecutors.diskIO().execute(() -> {
            long id = noteDao.insert(note);
            listener.onNoteCreated(id);
        });
    }

    @Override
    public void updateElement(String id, String content, Calendar editTime) {
        appExecutors.diskIO().execute(() -> noteDao.updateNoteContent(Integer.valueOf(id), content, editTime.getTimeInMillis()));
    }

    @Override
    public void deleteElement(String id) {
        appExecutors.diskIO().execute(() -> noteDao.flagDeleted(Integer.valueOf(id)));
    }

    @Override
    public Calendar getLastSyncedTime() {
        long lastSync = sharedPreferences.getLong(LAST_SYNC_KEY, 0);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(lastSync);
        return cal;
    }

    @Override
    public void setLastSyncedTime(Calendar calendar) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(LAST_SYNC_KEY, calendar.getTimeInMillis());
        editor.apply();
    }

    @Override
    public String getClientID() {
        return sharedPreferences.getString(CLIENT_ID_KEY, "");
    }

    @Override
    public void setClientID(String clientID) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CLIENT_ID_KEY, clientID);
        editor.apply();
    }

    public interface OnNoteCreatedListener {
        void onNoteCreated(long id);
    }
}
