package com.smlnotes.smlnotes.model.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.smlnotes.lib.ConnectionConfig;
import com.smlnotes.lib.SMLClient;
import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.screens.overview.OverviewFragment;

public class CredentialsManager {

    private static final String KEY_URL = "key_url";
    private static final String KEY_USER = "key_user";
    private static final String KEY_PASSWORD = "key_password";
    private static final String KEY_CERT = "key_cert";
    private static final String KEY_METHOD = "key_method";
    private final SharedPreferences sharedPreferences;

    private final SMLClient<?> smlClient;
    private final ConnectionConfigBuilder connectionConfigBuilder;

    public CredentialsManager(SharedPreferences sharedPreferences, SMLClient<?> smlClient, ConnectionConfigBuilder connectionConfigBuilder) {
        this.sharedPreferences = sharedPreferences;
        this.smlClient = smlClient;
        this.connectionConfigBuilder = connectionConfigBuilder;
    }

    public void storeCredentials(Credentials credentials) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_URL, credentials.getUrl());
        editor.putString(KEY_CERT, credentials.getCert());
        editor.putString(KEY_USER, credentials.getUser());
        editor.putString(KEY_PASSWORD, credentials.getPassword());
        editor.putString(KEY_METHOD, String.valueOf(credentials.getLoginMethod().ordinal()));
        editor.apply();
    }

    public Credentials getCredentials() {
        Credentials credentials = new Credentials();

        String method = sharedPreferences.getString(KEY_METHOD, "");
        try {
            int methodInt = Integer.valueOf(method);
            credentials.setLoginMethod(LoginMethod.values()[methodInt]);
        } catch (NumberFormatException ignored) {
        }
        credentials.setUrl(sharedPreferences.getString(KEY_URL, ""));
        credentials.setUser(sharedPreferences.getString(KEY_USER, ""));
        credentials.setPassword(sharedPreferences.getString(KEY_PASSWORD, ""));
        credentials.setCert(sharedPreferences.getString(KEY_CERT, ""));
        return credentials;
    }

    public boolean isLoggedIn() {
        String method = sharedPreferences.getString(KEY_METHOD, "");
        if (method.isEmpty())
            return false;
        switch (LoginMethod.values()[Integer.valueOf(method)]) {
            case CERT:
                return !sharedPreferences.getString(KEY_CERT, "").isEmpty();
            case USER_PASSWORD:
                return !sharedPreferences.getString(KEY_PASSWORD, "").isEmpty() && !sharedPreferences.getString(KEY_USER, "").isEmpty();
            case USER_PASSWORD_CERT:
                return !sharedPreferences.getString(KEY_PASSWORD, "").isEmpty() && !sharedPreferences.getString(KEY_USER, "").isEmpty() && !sharedPreferences.getString(KEY_CERT, "").isEmpty();
        }
        return false;
    }


    public void checkCredentials(Credentials credentials, Context context, CheckCallback checkCallback) {
        ConnectionConfig connectionConfig = connectionConfigBuilder.buildConnectionConfig(credentials, context);
        connectionConfig.testConfig(valid -> {
            if (valid) {
                smlClient.reconfigure(connectionConfig);
                checkCallback.onSuccess();
            } else {
                checkCallback.onError("Unable to authenticate.");
            }
        });
    }

    public void checkCurrentCredentials(Context context, CheckCallback checkCallback) {
        ConnectionConfig connectionConfig = connectionConfigBuilder.buildConnectionConfig(getCredentials(), context);
        connectionConfig.testConfig(valid -> {
            if (valid) {
                checkCallback.onSuccess();
            } else {
                checkCallback.onError("");
            }
        });
    }

    public void logout() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(KEY_PASSWORD);
        editor.remove(NotesRepository.CLIENT_ID_KEY);
        editor.remove(NotesRepository.LAST_SYNC_KEY);
        editor.remove(AutoSyncManager.KEY_AUTOMATIC_SYNC);
        editor.remove(OverviewFragment.KEY_FIRST_LOGIN);
        editor.commit();
    }

    public enum LoginMethod {
        USER_PASSWORD,
        CERT,
        USER_PASSWORD_CERT
    }

    public interface CheckCallback {
        void onSuccess();

        void onError(String err);
    }
}
