package com.smlnotes.smlnotes.model.network;

import android.content.SharedPreferences;

import androidx.lifecycle.Observer;

import com.smlnotes.smlnotes.common.AppExecutors;
import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;

import java.util.Calendar;
import java.util.List;

public class AutoSyncManager implements Observer<List<Note>> {
    public static final String KEY_AUTOMATIC_SYNC = "key_automatic_sync";

    private final NotesRepository notesRepository;
    private final SharedPreferences sharedPreferences;
    private final AppExecutors appExecutors;
    private final CredentialsManager credentialsManager;

    public AutoSyncManager(NotesRepository notesRepository, SharedPreferences sharedPreferences, AppExecutors appExecutors, CredentialsManager credentialsManager) {
        this.notesRepository = notesRepository;
        this.sharedPreferences = sharedPreferences;
        this.appExecutors = appExecutors;
        this.credentialsManager = credentialsManager;
    }

    public void init() {
        notesRepository.getNotes().observeForever(this);
    }

    @Override
    public void onChanged(List<Note> notes) {
        boolean autoSync = sharedPreferences.getBoolean(KEY_AUTOMATIC_SYNC, true);
        if (!autoSync || !credentialsManager.isLoggedIn())
            return;
        appExecutors.diskIO().execute(() -> {
            Calendar lastSynced = notesRepository.getLastSyncedTime();

            List<Note> changedElements = notesRepository.getChangedElements(lastSynced);
            boolean hasChangedElements = false;
            for (Note note : changedElements) {
                if (note.getContent().length() > 0) {
                    hasChangedElements = true;
                    break;
                }
            }

            boolean hasDeletedElements = notesRepository.getDeletedElements().size() > 0;

            if (hasChangedElements || hasDeletedElements) {
                notesRepository.syncNotes(false);
            }
        });
    }
}
