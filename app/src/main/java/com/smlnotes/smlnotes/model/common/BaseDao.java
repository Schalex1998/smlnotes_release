package com.smlnotes.smlnotes.model.common;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

public interface BaseDao<T> {

    @Insert(onConflict = REPLACE)
    long insert(T data);

    @Insert(onConflict = REPLACE)
    List<Long> insertAll(T... data);

    @Update
    void update(T data);

    @Delete
    void delete(T data);
}
