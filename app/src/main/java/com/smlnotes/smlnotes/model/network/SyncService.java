package com.smlnotes.smlnotes.model.network;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.smlnotes.lib.SMLClient;
import com.smlnotes.smlnotes.construction.service.BaseService;

import javax.inject.Inject;

public class SyncService extends BaseService {

    public static final int JOB_ID = 1231;
    public static final String BROADCAST = ".BROADCAST";
    public static final String DATA_SUCCESS = ".SUCCESS";
    public static final String DATA_ERROR_MESSAGE = ".ERROR_MESSAGE";

    @Inject
    SMLClient smlClient;

    public static void sync(Context context) {
        enqueueWork(context, SyncService.class, SyncService.JOB_ID, new Intent());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getServiceComponent().inject(this);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        smlClient.sync(() -> {
            Intent broadcastIntent = new Intent(BROADCAST).putExtra(DATA_SUCCESS, true).putExtra(DATA_ERROR_MESSAGE, "");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> Toast.makeText(SyncService.this.getApplicationContext(), "Synchronisation successful.", Toast.LENGTH_SHORT).show());
        }, (reason, error) -> {
            Intent broadcastIntent = new Intent(BROADCAST).putExtra(DATA_SUCCESS, false).putExtra(DATA_ERROR_MESSAGE, reason);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        });
    }

    @Override
    public boolean onStopCurrentWork() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
