package com.smlnotes.smlnotes.model.logging;

import android.util.Log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;

public class StringLogger extends UnsynchronizedAppenderBase<ILoggingEvent> {

    private PatternLayoutEncoder encoder = null;
    private StringBuffer stringBuffer = new StringBuffer();

    @Override
    protected void append(ILoggingEvent event) {
        if (!isStarted()) {
            return;
        }

        String message = this.encoder.getLayout().doLayout(event);
        stringBuffer.append(message);
    }

    public String getLog(){
        return stringBuffer.toString();
    }

    public PatternLayoutEncoder getEncoder() {
        return encoder;
    }

    public void setEncoder(PatternLayoutEncoder encoder) {
        this.encoder = encoder;
    }

    public void clear() {
        stringBuffer = new StringBuffer();
    }
}
