package com.smlnotes.smlnotes.model.network;

import android.content.Context;

import com.smlnotes.lib.ConnectionConfig;
import com.smlnotes.lib.auth.AbstractAuth;
import com.smlnotes.lib.auth.BasicAndCertAuth;
import com.smlnotes.lib.auth.BasicSyncMLAuth;
import com.smlnotes.lib.auth.CertAuth;
import com.smlnotes.lib.auth.NoAuth;

public class ConnectionConfigBuilder {

    public ConnectionConfig buildConnectionConfig(Credentials credentials, Context context) {
        AbstractAuth auth = new NoAuth();

        if (credentials.getLoginMethod() == CredentialsManager.LoginMethod.USER_PASSWORD) {
            auth = new BasicSyncMLAuth(credentials.getUser(), credentials.getPassword());
        } else if (credentials.getLoginMethod() == CredentialsManager.LoginMethod.CERT) {
            auth = new CertAuth(credentials.getCert(), context);
        } else if (credentials.getLoginMethod() == CredentialsManager.LoginMethod.USER_PASSWORD_CERT) {
            BasicSyncMLAuth basicAuth = new BasicSyncMLAuth(credentials.getUser(), credentials.getPassword());
            CertAuth certAuth = new CertAuth(credentials.getCert(), context);
            auth = BasicAndCertAuth.builder()
                    .basicAuth(basicAuth)
                    .certAuth(certAuth)
                    .build();
        }

        auth.setCertWhiteList(new CertWhiteListHandler(context));


        ConnectionConfig connectionConfig = ConnectionConfig.builder()
                .targetURL(credentials.getUrl())
                .syncTarget("note")
                .auth(auth)
                .build();

        return connectionConfig;
    }
}
