package com.smlnotes.smlnotes.model.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.smlnotes.smlnotes.model.common.BaseDao;

import java.util.List;

@Dao
public interface NoteDao extends BaseDao<Note> {

    @Query("SELECT * FROM notes ORDER BY lastChanged DESC")
    LiveData<List<Note>> loadAll();

    @Query("SELECT * FROM notes WHERE NOT deleted ORDER BY lastChanged DESC")
    LiveData<List<Note>> loadAllNotDeleted();

    @Query("SELECT * FROM notes WHERE NOT deleted AND content LIKE '%' || :searchTerm || '%' ORDER BY lastChanged DESC")
    LiveData<List<Note>> loadAllNotDeletedMatching(String searchTerm);

    @Query("SELECT id FROM notes WHERE deleted ORDER BY lastChanged DESC")
    List<Integer> getDeletedIds();

    @Query("UPDATE notes SET local = 0 WHERE id = :id")
    void flagNotLocal(int id);

    @Query("UPDATE notes SET deleted = 1 WHERE id = :id")
    void flagDeleted(int id);

    @Query("SELECT * FROM notes WHERE lastChanged > :lastChanged ORDER BY lastChanged DESC")
    List<Note> loadAllAfter(long lastChanged);

    @Query("SELECT * FROM notes WHERE id = :id LIMIT 1")
    LiveData<Note> load(int id);

    @Query("DELETE FROM notes WHERE id = :id")
    void delete(int id);

    @Query("UPDATE notes SET content = :content, lastChanged = :lastChanged WHERE id = :id")
    void updateNoteContent(int id, String content, long lastChanged);

    @Query("DELETE FROM notes WHERE NOT local")
    void clearSyncedNotes();
}
