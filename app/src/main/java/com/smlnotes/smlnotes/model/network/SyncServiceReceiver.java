package com.smlnotes.smlnotes.model.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SyncServiceReceiver extends BroadcastReceiver {

    private final OnSyncCompletedListener onSyncCompletedListener;

    public SyncServiceReceiver(OnSyncCompletedListener onSyncCompletedListener) {
        super();
        this.onSyncCompletedListener = onSyncCompletedListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        onSyncCompletedListener.onSyncCompleted(intent.getBooleanExtra(SyncService.DATA_SUCCESS, false), intent.getStringExtra(SyncService.DATA_ERROR_MESSAGE));
    }

    public interface OnSyncCompletedListener {
        void onSyncCompleted(boolean success, String error);
    }
}
