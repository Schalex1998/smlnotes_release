package com.smlnotes.smlnotes.model.network;

public class Credentials {
    private CredentialsManager.LoginMethod loginMethod;
    private String user;
    private String password;
    private String cert;
    private String url;

    public Credentials() {
    }

    public CredentialsManager.LoginMethod getLoginMethod() {
        return loginMethod;
    }

    public void setLoginMethod(CredentialsManager.LoginMethod loginMethod) {
        this.loginMethod = loginMethod;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}