package com.smlnotes.smlnotes.model.network;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.smlnotes.lib.auth.WhitelistTrustManager;
import com.smlnotes.smlnotes.screens.activity.CertActivity;
import com.smlnotes.smlnotes.screens.activity.SettingsActivity;

import java.util.HashSet;
import java.util.Set;

public class CertWhiteListHandler implements WhitelistTrustManager.CertWhiteList {

    private final Context context;
    public static final String TRUSTED_CERTS_KEY = "key_trusted_certs";
    private final SharedPreferences sharedPreferences;


    public CertWhiteListHandler(Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    @Override
    public boolean isWhitelisted(String thumbprint) {
        Set<String> trustedCerts = sharedPreferences.getStringSet(TRUSTED_CERTS_KEY, new HashSet<>());
        boolean match = trustedCerts != null && trustedCerts.contains(thumbprint);

        if (!match) {
            Intent intent = new Intent(context, CertActivity.class);
            intent.putExtra(CertActivity.DATA_THUMBPRINT, thumbprint);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

        return match;
    }
}
