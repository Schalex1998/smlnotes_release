package com.smlnotes.smlnotes.model.database;


import android.text.format.DateFormat;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.smlnotes.lib.Syncable;

import java.util.Calendar;
import java.util.Locale;

@Entity(tableName = "notes")
public class Note implements Syncable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private long lastChanged;
    private String content;
    private boolean local;
    private boolean deleted;

    public Note(int id, long lastChanged, String content, boolean local, boolean deleted) {
        this.id = id;
        this.lastChanged = lastChanged;
        this.content = content;
        this.local = local;
        this.deleted = deleted;
    }

    @Ignore
    public Note(long lastChanged, String content, boolean local, boolean deleted) {
        this.lastChanged = lastChanged;
        this.content = content;
        this.local = local;
        this.deleted = deleted;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public Calendar getLastEdited() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(lastChanged);
        return cal;
    }

    @Override
    public boolean isLocalOnly() {
        return isLocal();
    }

    public long getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(long lastChanged) {
        this.lastChanged = lastChanged;
    }

    public String getLastChangedFormatted() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(lastChanged);
        return DateFormat.format("MM/dd/yy HH:mm", calendar).toString();
    }
}
