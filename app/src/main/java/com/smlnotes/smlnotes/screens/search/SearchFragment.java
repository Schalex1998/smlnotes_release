package com.smlnotes.smlnotes.screens.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.ViewFactory;
import com.smlnotes.smlnotes.screens.common.fragment.DefaultFragment;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ViewModelFactory;
import com.smlnotes.smlnotes.screens.notedetail.NoteDetailFragment;
import com.smlnotes.smlnotes.screens.search.searchview.SearchView;

import java.util.List;

import javax.inject.Inject;

public class SearchFragment extends DefaultFragment implements SearchView.SearchViewListener {

    @Inject
    ViewFactory viewFactory;
    @Inject
    ViewModelFactory viewModelFactory;

    private SearchView searchView;
    private SearchViewModel searchViewModel;

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        searchView = viewFactory.newInstance(SearchView.class, container);
        return searchView.getRootView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel.class);
        searchViewModel.init(null);
        searchViewModel.getNotes().observe(this, this::onNotesFetched);
    }

    @Override
    public void onStart() {
        super.onStart();
        searchView.registerListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        searchView.unregisterListener(this);
    }

    @Nullable
    @Override
    public EncapsulatedFragmentView getEncapsulatedView() {
        return searchView;
    }

    @Override
    public void onNoteSelected(Note note) {
        navigateTo(NoteDetailFragment.newInstance(note.getId()), true, false, true);
    }

    @Override
    public void onSearch(String searchString) {
        search(searchString);
    }

    private void search(String searchString) {
        searchViewModel.init(searchString);
        searchViewModel.getNotes().observe(this, this::onNotesFetched);
    }

    private void onNotesFetched(List<Note> notes) {
        searchView.bind(notes);
    }
}
