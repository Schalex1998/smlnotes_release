package com.smlnotes.smlnotes.screens.overview;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.model.network.SyncService;
import com.smlnotes.smlnotes.model.network.SyncServiceReceiver;
import com.smlnotes.smlnotes.screens.ViewFactory;
import com.smlnotes.smlnotes.screens.activity.SettingsActivity;
import com.smlnotes.smlnotes.screens.common.fragment.DefaultFragment;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ViewModelFactory;
import com.smlnotes.smlnotes.screens.notedetail.NoteDetailFragment;
import com.smlnotes.smlnotes.screens.overview.overviewview.OverviewView;
import com.smlnotes.smlnotes.screens.search.SearchFragment;

import java.util.List;

import javax.inject.Inject;

public class OverviewFragment extends DefaultFragment implements OverviewView.OverviewViewListener {

    @Inject
    ViewFactory viewFactory;
    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    NotesRepository notesRepository;

    private OverviewView overviewView;
    private OverviewViewModel overviewViewModel;

    public static final String KEY_FIRST_LOGIN = "key_first_login";

    public static OverviewFragment newInstance() {
        return new OverviewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);

        IntentFilter intentFilter = new IntentFilter(SyncService.BROADCAST);
        SyncServiceReceiver syncServiceReceiver = new SyncServiceReceiver((success, error) -> {
            if (overviewView != null)
                overviewView.hideProgress();
            if (!success) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(syncServiceReceiver, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        overviewView = viewFactory.newInstance(OverviewView.class, container);
        return overviewView.getRootView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        overviewViewModel = ViewModelProviders.of(this, viewModelFactory).get(OverviewViewModel.class);
        overviewViewModel.init();
        overviewViewModel.getNotes().observe(this, this::onNotesFetched);
    }

    @Override
    public void onStart() {
        super.onStart();
        overviewView.registerListener(this);
        checkFirstLoginAndSync();
    }

    private void checkFirstLoginAndSync() {
        boolean firstLogin = sharedPreferences.getBoolean(KEY_FIRST_LOGIN, true);
        if (firstLogin) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_FIRST_LOGIN, false);
            editor.commit();
            onSync();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                onSync();
                return true;
            case R.id.action_settings:
                navigateTo(SettingsActivity.class, false);
                return true;
            case R.id.action_add:
                overviewViewModel.createNewNote(id -> navigateTo(NoteDetailFragment.newInstance((int) id), true, false, true));
                return true;
            case R.id.action_search:
                navigateTo(SearchFragment.newInstance(), true, false, false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        overviewView.unregisterListener(this);
    }

    @Nullable
    @Override
    public EncapsulatedFragmentView getEncapsulatedView() {
        return overviewView;
    }

    @Override
    public void onNoteSelected(Note note) {
        navigateTo(NoteDetailFragment.newInstance(note.getId()), true, false, true);
    }

    private void onSync() {
        overviewView.showProgress();
        overviewViewModel.syncNotes();
    }

    private void onNotesFetched(List<Note> notes) {
        overviewView.bind(notes);
    }
}
