package com.smlnotes.smlnotes.screens.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.model.network.CredentialsManager;
import com.smlnotes.smlnotes.screens.common.activity.DefaultFragmentContainerActivity;
import com.smlnotes.smlnotes.screens.login.LoginFragment;
import com.smlnotes.smlnotes.screens.overview.OverviewFragment;
import com.smlnotes.smlnotes.screens.search.searchview.SearchView;

import javax.inject.Inject;

public class MainActivity extends DefaultFragmentContainerActivity {

    @Inject
    CredentialsManager credentialsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));

        getSupportFragmentManager().addOnBackStackChangedListener(this::showOrHideSearchBar);

        if (savedInstanceState == null) {
            if (credentialsManager.isLoggedIn()) {
                navigateTo(OverviewFragment.newInstance(), false, false, false);
            } else {
                navigateTo(LoginFragment.newInstance(), false, false, false);
            }
        }

        setSearchListener();
        showOrHideSearchBar();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setSearchListener() {
        TextInputEditText searchText = findViewById(R.id.et_search);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (currentFragment instanceof SearchView.SearchViewListener)
                    ((SearchView.SearchViewListener) currentFragment).onSearch(searchText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void showOrHideSearchBar() {
        this.hideKeyboard();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (currentFragment instanceof SearchView.SearchViewListener) {
            findViewById(R.id.cv_search).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cv_search).setVisibility(View.GONE);
        }
    }

    @Override
    public int getContainer() {
        return R.id.container;
    }

    @Override
    public void setTitle(String title) {
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.ctl);
        collapsingToolbarLayout.setTitle(title);
    }

    @Override
    public void showActionBar(boolean show) {
        AppBarLayout appBarLayout = findViewById(R.id.abl);
        if (show) {
            appBarLayout.setVisibility(View.VISIBLE);
        } else {
            appBarLayout.setVisibility(View.GONE);
        }
    }
}
