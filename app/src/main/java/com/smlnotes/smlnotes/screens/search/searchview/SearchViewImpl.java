package com.smlnotes.smlnotes.screens.search.searchview;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.databinding.FragmentSearchBinding;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.adapter.DefaultItemAdapter;
import com.smlnotes.smlnotes.screens.common.adapter.ItemAdapter;
import com.smlnotes.smlnotes.screens.common.view.DefaultEncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.overview.noteitemview.NoteItemView;
import com.smlnotes.smlnotes.screens.overview.noteitemview.NoteItemViewSupplier;

import java.util.List;

public class SearchViewImpl extends DefaultEncapsulatedFragmentView<SearchView.SearchViewListener> implements SearchView, NoteItemView.NoteItemViewListener {

    private FragmentSearchBinding viewBinding;
    private ItemAdapter<Note> itemAdapter;

    public SearchViewImpl(LayoutInflater inflater, ViewGroup container) {
        viewBinding = FragmentSearchBinding.inflate(inflater, container, false);
        setRootView(viewBinding.getRoot());

        viewBinding.srl.setEnabled(false);
        viewBinding.rv.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        itemAdapter = new DefaultItemAdapter<>(new NoteItemViewSupplier(this));
        viewBinding.rv.setAdapter(itemAdapter.getAdapter());
    }

    @Override
    public String getTitle() {
        return getContext().getString(R.string.app_name);
    }

    @Override
    public void showProgress() {
        viewBinding.srl.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        viewBinding.srl.setRefreshing(false);
    }

    @Override
    public void bind(List<Note> notes) {
        itemAdapter.setItems(notes);
    }

    @Override
    public void onNoteSelected(Note note) {
        forEachListener(searchViewListener -> searchViewListener.onNoteSelected(note));
    }
}
