package com.smlnotes.smlnotes.screens.common.view;

import android.view.View;

public interface EncapsulatedView {

    View getRootView();
}
