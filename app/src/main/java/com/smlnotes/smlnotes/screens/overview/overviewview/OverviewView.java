package com.smlnotes.smlnotes.screens.overview.overviewview;

import com.smlnotes.smlnotes.common.EventEmitter;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ProgressView;

import java.util.List;

public interface OverviewView extends EncapsulatedFragmentView, ProgressView, EventEmitter<OverviewView.OverviewViewListener> {

    void bind(List<Note> notes);

    interface OverviewViewListener {
        void onNoteSelected(Note note);
    }
}
