package com.smlnotes.smlnotes.screens.notedetail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;

public class NoteDetailViewModel extends ViewModel {

    private final NotesRepository notesRepository;
    private LiveData<Note> note;

    public NoteDetailViewModel(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    void init(int id) {
        if (this.note != null) {
            return;
        }
        note = notesRepository.getNote(id);
    }

    void updateContent(String content) {
        Note note = this.note.getValue();
        if (note != null && note.getContent() != null && (note.getContent().isEmpty() || !note.getContent().equals(content))) {
            note.setContent(content);
            notesRepository.updateNote(note);
        }
    }

    LiveData<Note> getNote() {
        return note;
    }
}
