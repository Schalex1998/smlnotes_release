package com.smlnotes.smlnotes.screens.login.loginview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.databinding.FragmentLoginBinding;
import com.smlnotes.smlnotes.model.network.Credentials;
import com.smlnotes.smlnotes.model.network.CredentialsManager;
import com.smlnotes.smlnotes.screens.common.view.DefaultEncapsulatedFragmentView;

public class LoginViewImpl extends DefaultEncapsulatedFragmentView<LoginView.LoginViewListener> implements LoginView {

    private FragmentLoginBinding viewBinding;

    public LoginViewImpl(LayoutInflater inflater, ViewGroup container) {
        viewBinding = FragmentLoginBinding.inflate(inflater, container, false);
        setRootView(viewBinding.getRoot());

        viewBinding.bLogin.setOnClickListener(v -> forEachListener(LoginViewListener::onLogin));
        viewBinding.bCert.setOnClickListener(v -> forEachListener(LoginViewListener::onSelectCertificate));
        viewBinding.sMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateUi(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewBinding.bSendLogs.setOnClickListener(v -> forEachListener(LoginViewListener::onSendLogs));
    }

    private void updateUi(int methodOrd) {
        CredentialsManager.LoginMethod method = CredentialsManager.LoginMethod.values()[methodOrd];
        viewBinding.tilUsername.setVisibility(View.VISIBLE);
        viewBinding.tilPassword.setVisibility(View.VISIBLE);
        viewBinding.bCert.setVisibility(View.VISIBLE);
        viewBinding.tvCert.setVisibility(View.VISIBLE);
        switch (method) {
            case CERT:
                viewBinding.tilUsername.setVisibility(View.GONE);
                viewBinding.tilPassword.setVisibility(View.GONE);
                break;
            case USER_PASSWORD:
                viewBinding.bCert.setVisibility(View.GONE);
                viewBinding.tvCert.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public Bundle getViewState() {
        Bundle bundle = new Bundle();
        bundle.putInt(VIEW_STATE_METHOD, viewBinding.sMethod.getSelectedItemPosition());
        bundle.putString(VIEW_STATE_CERT, viewBinding.tvCert.getText().toString());
        if (viewBinding.etUrl.getText() != null)
            bundle.putString(VIEW_STATE_URL, viewBinding.etUrl.getText().toString());
        if (viewBinding.etUsername.getText() != null)
            bundle.putString(VIEW_STATE_USER, viewBinding.etUsername.getText().toString());
        if (viewBinding.etPassword.getText() != null)
            bundle.putString(VIEW_STATE_PASSWORD, viewBinding.etPassword.getText().toString());
        return bundle;
    }

    @Override
    public void showProgress() {
        viewBinding.pb.setVisibility(View.VISIBLE);
        viewBinding.etPassword.setEnabled(false);
        viewBinding.etUsername.setEnabled(false);
        viewBinding.etUrl.setEnabled(false);
        viewBinding.bCert.setEnabled(false);
        viewBinding.bLogin.setEnabled(false);
        viewBinding.sMethod.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        viewBinding.pb.setVisibility(View.INVISIBLE);
        viewBinding.etPassword.setEnabled(true);
        viewBinding.etUsername.setEnabled(true);
        viewBinding.etUrl.setEnabled(true);
        viewBinding.bCert.setEnabled(true);
        viewBinding.bLogin.setEnabled(true);
        viewBinding.sMethod.setEnabled(true);
    }

    @Override
    public void showCertAlias(String alias) {
        viewBinding.tvCert.setText(alias);
    }

    @Override
    public void bind(Credentials credentials) {
        if (credentials == null)
            return;
        if (credentials.getLoginMethod() != null)
            viewBinding.sMethod.setSelection(credentials.getLoginMethod().ordinal());
        if (credentials.getUrl() != null && !credentials.getUrl().isEmpty())
            viewBinding.etUrl.setText(credentials.getUrl());
        if (credentials.getUser() != null && !credentials.getUser().isEmpty())
            viewBinding.etUsername.setText(credentials.getUser());
        if (credentials.getCert() != null && !credentials.getCert().isEmpty())
            viewBinding.tvCert.setText(credentials.getCert());
    }

    @Override
    public String getTitle() {
        return getContext().getString(R.string.fragment_login_title);
    }
}
