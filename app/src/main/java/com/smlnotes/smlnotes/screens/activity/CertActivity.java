package com.smlnotes.smlnotes.screens.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.model.network.CertWhiteListHandler;

import java.util.HashSet;
import java.util.Set;

public class CertActivity extends AppCompatActivity {
    public static final String DATA_THUMBPRINT = ".THUMBPRINT";
    private Button allow;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert);

        allow = findViewById(R.id.b_allow);
        Button deny = findViewById(R.id.b_deny);
        text = findViewById(R.id.tv_cert);
        deny.setOnClickListener(v -> CertActivity.this.onBackPressed());

        onCertCheck(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        onCertCheck(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void onCertCheck(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(DATA_THUMBPRINT)) {
            String thumbprint = extras.getString(DATA_THUMBPRINT);
            text.setText(getString(R.string.trust, thumbprint));
            allow.setOnClickListener(v -> trustCert(thumbprint));
        }
    }

    private void trustCert(String thumbprint) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Set<String> trustedCerts = sharedPreferences.getStringSet(CertWhiteListHandler.TRUSTED_CERTS_KEY, new HashSet<>());
        trustedCerts.add(thumbprint);
        editor.putStringSet(CertWhiteListHandler.TRUSTED_CERTS_KEY, trustedCerts);
        editor.commit();
        onBackPressed();
    }

}
