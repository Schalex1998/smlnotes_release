package com.smlnotes.smlnotes.screens.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;

import java.util.List;

public class OverviewViewModel extends ViewModel {

    private final NotesRepository notesRepository;
    private LiveData<List<Note>> notes;

    public OverviewViewModel(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    void init() {
        if (this.notes != null) {
            return;
        }
        notes = notesRepository.getNotDeletedNotes();
    }

    void syncNotes() {
        notesRepository.syncNotes(true);
    }

    void createNewNote(NotesRepository.OnNoteCreatedListener listener) {
        notesRepository.createElementLocal(listener);
    }

    LiveData<List<Note>> getNotes() {
        return notes;
    }
}
