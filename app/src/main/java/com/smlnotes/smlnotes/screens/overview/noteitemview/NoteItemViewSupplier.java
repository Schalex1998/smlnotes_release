package com.smlnotes.smlnotes.screens.overview.noteitemview;

import android.view.ViewGroup;

import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.ItemView;
import com.smlnotes.smlnotes.screens.common.view.ItemViewSupplier;

public class NoteItemViewSupplier implements ItemViewSupplier<Note> {

    private final NoteItemView.NoteItemViewListener listener;

    public NoteItemViewSupplier(NoteItemView.NoteItemViewListener listener) {
        this.listener = listener;
    }

    @Override
    public ItemView<Note> supply(ViewGroup parent) {
        NoteItemView noteItemView = new NoteItemViewImpl(parent);
        noteItemView.registerListener(listener);
        return noteItemView;
    }
}
