package com.smlnotes.smlnotes.screens.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.security.KeyChain;
import android.text.format.DateFormat;
import android.widget.Toast;

import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;

import com.smlnotes.lib.ConnectionConfig;
import com.smlnotes.lib.SMLClient;
import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.construction.presentation.BaseSettingsFragment;
import com.smlnotes.smlnotes.model.logging.StringLogger;
import com.smlnotes.smlnotes.model.network.AutoSyncManager;
import com.smlnotes.smlnotes.model.network.CertWhiteListHandler;
import com.smlnotes.smlnotes.model.network.ConnectionConfigBuilder;
import com.smlnotes.smlnotes.model.network.Credentials;
import com.smlnotes.smlnotes.model.network.CredentialsManager;
import com.smlnotes.smlnotes.screens.activity.MainActivity;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

public class PreferenceFragment extends BaseSettingsFragment {

    private EditTextPreference username;
    private EditTextPreference password;
    private Preference certificate;

    @Inject
    AutoSyncManager autoSyncManager;
    @Inject
    SMLClient smlClient;
    @Inject
    CredentialsManager credentialsManager;
    @Inject
    ConnectionConfigBuilder connectionConfigBuilder;
    @Inject
    StringLogger stringLogger;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        username = findPreference("key_user");
        password = findPreference("key_password");
        certificate = findPreference("key_cert");
        ListPreference method = findPreference("key_method");
        Preference logout = findPreference("logout");
        Preference check = findPreference("check");
        Preference untrust = findPreference("untrust");
        Preference lastSync = findPreference("key_last_sync");
        Preference clientId = findPreference("key_client_id");
        Preference clearLogs = findPreference("key_clear_logs");
        Preference sendLogs = findPreference("key_send_logs");

        lastSync.setSummaryProvider(preference -> {
            long timestamp = preference.getSharedPreferences().getLong(preference.getKey(), 0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp);
            return DateFormat.format("MM/dd/yy HH:mm", calendar).toString();
        });

        clientId.setSummaryProvider(preference -> preference.getSharedPreferences().getString(preference.getKey(), ""));

        logout.setOnPreferenceClickListener(preference -> {
            logout();
            return true;
        });

        check.setOnPreferenceClickListener(preference -> {
            checkAuthSettings();
            return true;
        });

        untrust.setOnPreferenceClickListener(preference -> {
            SharedPreferences sharedPreferences = preference.getSharedPreferences();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(CertWhiteListHandler.TRUSTED_CERTS_KEY);
            editor.commit();
            if (getActivity() != null)
                getActivity().runOnUiThread(() -> {
                    Toast.makeText(getActivity(), "Untrusted all certificates.", Toast.LENGTH_SHORT).show();
                });
            return true;
        });

        certificate.setOnPreferenceClickListener(preference -> {
            KeyChain.choosePrivateKeyAlias(getActivity(), alias -> {
                certificate.getSharedPreferences().edit().putString(certificate.getKey(), alias).apply();
                certificate.setSummary(alias);
            }, null, null, null, -1, null);
            return true;
        });

        certificate.setSummary(certificate.getSharedPreferences().getString(certificate.getKey(), ""));

        method.setOnPreferenceChangeListener((preference, newValue) -> {
            updateAuthPreferences(Integer.valueOf(newValue.toString()));
            return true;
        });

        clearLogs.setOnPreferenceClickListener(preference -> {
            clearLogs();
            return true;
        });

        sendLogs.setOnPreferenceClickListener(preference -> {
            sendLogs();
            return true;
        });

        updateAuthPreferences(Integer.valueOf(method.getValue()));
    }

    private void logout() {
        credentialsManager.logout();

        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        changeAuthSettings();
    }

    private void updateAuthPreferences(int selected) {
        username.setVisible(selected == 0 || selected == 2);
        password.setVisible(selected == 0 || selected == 2);
        certificate.setVisible(selected == 1 || selected == 2);
    }

    private void changeAuthSettings() {
        if (credentialsManager.isLoggedIn() && getContext() != null) {
            Credentials credentials = credentialsManager.getCredentials();
            ConnectionConfig connectionConfig = connectionConfigBuilder.buildConnectionConfig(credentials, getContext().getApplicationContext());
            smlClient.reconfigure(connectionConfig);
        }
    }

    private void checkAuthSettings() {
        credentialsManager.checkCurrentCredentials(getContext(), new CredentialsManager.CheckCallback() {
            @Override
            public void onSuccess() {
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getActivity(), "Authentication successful.", Toast.LENGTH_SHORT).show();
                    });
            }

            @Override
            public void onError(String err) {
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getActivity(), "Authentication failed!", Toast.LENGTH_SHORT).show();
                    });
            }
        });
    }

    private void clearLogs() {
        stringLogger.clear();
    }

    private void sendLogs() {
        String log = stringLogger.getLog();

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SMLNotes Logs");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, log);
        startActivity(Intent.createChooser(sharingIntent, "Share Logs"));
    }
}
