package com.smlnotes.smlnotes.screens.notedetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ShareCompat;
import androidx.lifecycle.ViewModelProviders;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.ViewFactory;
import com.smlnotes.smlnotes.screens.common.fragment.DefaultFragment;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ViewModelFactory;
import com.smlnotes.smlnotes.screens.notedetail.notedetailview.NoteDetailView;

import javax.inject.Inject;

public class NoteDetailFragment extends DefaultFragment implements NoteDetailView.NoteDetailViewListener {

    private static final String ARG_ID = "arg_id";
    @Inject
    ViewFactory viewFactory;
    @Inject
    ViewModelFactory viewModelFactory;
    private NoteDetailViewModel noteDetailViewModel;
    private NoteDetailView noteDetailView;
    private int id;

    public static NoteDetailFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(ARG_ID, id);
        NoteDetailFragment noteDetailFragment = new NoteDetailFragment();
        noteDetailFragment.setArguments(args);
        return noteDetailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);

        if (getArguments() != null) {
            id = getArguments().getInt(ARG_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        noteDetailView = viewFactory.newInstance(NoteDetailView.class, container);
        return noteDetailView.getRootView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        noteDetailViewModel = ViewModelProviders.of(this, viewModelFactory).get(NoteDetailViewModel.class);
        noteDetailViewModel.init(id);
        noteDetailViewModel.getNote().observe(this, this::onNoteFetched);
    }

    @Override
    public void onStart() {
        super.onStart();
        noteDetailView.registerListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                noteDetailView.clearText();
                navigateBack();
                return true;
            case R.id.action_share:
                Note note = noteDetailViewModel.getNote().getValue();
                ShareCompat.IntentBuilder.from(getActivity()).setText(note.getContent()).setType("text/plain").setSubject(getString(R.string.share_subject, note.getLastChangedFormatted())).startChooser();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        noteDetailView.unregisterListener(this);
        if (noteDetailView.getViewState() != null) {
            noteDetailViewModel.updateContent(noteDetailView.getViewState().getString(NoteDetailView.VIEW_STATE_CONTENT));
        }
    }

    @Nullable
    @Override
    public EncapsulatedFragmentView getEncapsulatedView() {
        return noteDetailView;
    }

    private void onNoteFetched(Note note) {
        noteDetailView.bind(note);
    }
}
