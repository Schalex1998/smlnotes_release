package com.smlnotes.smlnotes.screens.common.view;

public interface ItemView<ItemType> extends EncapsulatedView {

    void bind(ItemType item);
}
