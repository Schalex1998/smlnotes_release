package com.smlnotes.smlnotes.screens.common.view;

public interface ProgressView {

    void showProgress();

    void hideProgress();
}
