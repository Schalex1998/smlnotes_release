package com.smlnotes.smlnotes.screens.overview.noteitemview;

import com.smlnotes.smlnotes.common.EventEmitter;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.ItemView;

public interface NoteItemView extends ItemView<Note>, EventEmitter<NoteItemView.NoteItemViewListener> {

    void onNoteClicked();

    interface NoteItemViewListener {
        void onNoteSelected(Note note);
    }
}
