package com.smlnotes.smlnotes.screens.activity;

import android.os.Bundle;
import android.view.MenuItem;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.screens.common.activity.DefaultNavigationActivity;
import com.smlnotes.smlnotes.screens.settings.PreferenceFragment;

public class SettingsActivity extends DefaultNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.settings);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new PreferenceFragment())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
