package com.smlnotes.smlnotes.screens.notedetail.notedetailview;

import com.smlnotes.smlnotes.common.EventEmitter;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;

public interface NoteDetailView extends EncapsulatedFragmentView, EventEmitter<NoteDetailView.NoteDetailViewListener> {

    String VIEW_STATE_CONTENT = "view_state_content";

    void bind(Note note);

    void clearText();

    interface NoteDetailViewListener {

    }
}
