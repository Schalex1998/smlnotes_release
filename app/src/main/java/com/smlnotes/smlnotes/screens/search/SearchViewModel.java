package com.smlnotes.smlnotes.screens.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;

import java.util.List;

public class SearchViewModel extends ViewModel {

    private final NotesRepository notesRepository;
    private LiveData<List<Note>> notes;
    private String cachedSearchTerm;

    public SearchViewModel(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    void init(String searchTerm) {
        if (searchTerm == null && cachedSearchTerm != null && !cachedSearchTerm.isEmpty()) {
            return;
        }
        if (searchTerm == null) {
            notes = notesRepository.getNotDeletedNotes();
            return;
        }
        if (searchTerm.equalsIgnoreCase(cachedSearchTerm)) {
            return;
        }
        cachedSearchTerm = searchTerm;
        notes = notesRepository.getNotDeletedNotesMatching(searchTerm);
    }

    LiveData<List<Note>> getNotes() {
        return notes;
    }
}
