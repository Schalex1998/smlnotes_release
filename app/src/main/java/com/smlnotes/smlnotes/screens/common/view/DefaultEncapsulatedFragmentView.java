package com.smlnotes.smlnotes.screens.common.view;

import android.os.Bundle;

import androidx.annotation.Nullable;

public abstract class DefaultEncapsulatedFragmentView<ListenerType> extends DefaultEncapsulatedView<ListenerType> implements EncapsulatedFragmentView {

    @Override
    public String getTitle() {
        return " ";
    }

    @Override
    public boolean shouldShowActionBar() {
        return true;
    }

    @Override
    public int getOptionsMenu() {
        return 0;
    }

    @Nullable
    @Override
    public Bundle getViewState() {
        return null;
    }
}
