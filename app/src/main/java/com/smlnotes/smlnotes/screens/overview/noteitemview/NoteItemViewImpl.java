package com.smlnotes.smlnotes.screens.overview.noteitemview;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.smlnotes.smlnotes.databinding.ItemViewNoteBinding;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.DefaultEncapsulatedView;

public class NoteItemViewImpl extends DefaultEncapsulatedView<NoteItemView.NoteItemViewListener> implements NoteItemView {

    private ItemViewNoteBinding binding;
    private Note note;

    NoteItemViewImpl(ViewGroup parent) {
        binding = ItemViewNoteBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        setRootView(binding.getRoot());
        binding.setNoteview(this);
    }

    @Override
    public void bind(Note item) {
        note = item;
        binding.setNote(item);
    }

    @Override
    public void onNoteClicked() {
        forEachListener(noteItemViewListener -> noteItemViewListener.onNoteSelected(note));
    }
}
