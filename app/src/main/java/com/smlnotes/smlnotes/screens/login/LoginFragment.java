package com.smlnotes.smlnotes.screens.login;

import android.content.Intent;
import android.os.Bundle;
import android.security.KeyChain;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.smlnotes.smlnotes.common.DialogUtil;
import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.logging.StringLogger;
import com.smlnotes.smlnotes.model.network.Credentials;
import com.smlnotes.smlnotes.model.network.CredentialsManager;
import com.smlnotes.smlnotes.screens.ViewFactory;
import com.smlnotes.smlnotes.screens.common.fragment.DefaultFragment;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.login.loginview.LoginView;
import com.smlnotes.smlnotes.screens.overview.OverviewFragment;

import javax.inject.Inject;

public class LoginFragment extends DefaultFragment implements LoginView.LoginViewListener {

    @Inject
    DialogUtil dialogUtil;
    @Inject
    ViewFactory viewFactory;
    @Inject
    CredentialsManager credentialsManager;
    @Inject
    NotesRepository notesRepository;
    @Inject
    StringLogger stringLogger;

    private LoginView loginView;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresentationComponent().inject(this);
        notesRepository.clearSyncedNotes();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        loginView = viewFactory.newInstance(LoginView.class, container);
        return loginView.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        loginView.registerListener(this);
        loginView.bind(credentialsManager.getCredentials());
    }

    @Override
    public void onStop() {
        super.onStop();
        loginView.unregisterListener(this);
    }

    @Nullable
    @Override
    public EncapsulatedFragmentView getEncapsulatedView() {
        return loginView;
    }

    @Override
    public void onLogin() {
        loginView.showProgress();
        Bundle state = loginView.getViewState();
        if (state != null) {
            Credentials credentials = new Credentials();
            credentials.setLoginMethod(CredentialsManager.LoginMethod.values()[state.getInt(LoginView.VIEW_STATE_METHOD)]);
            credentials.setCert(state.getString(LoginView.VIEW_STATE_CERT, ""));
            credentials.setUser(state.getString(LoginView.VIEW_STATE_USER, ""));
            credentials.setPassword(state.getString(LoginView.VIEW_STATE_PASSWORD, ""));
            credentials.setUrl(state.getString(LoginView.VIEW_STATE_URL, ""));
            CredentialsManager.CheckCallback checkCallback = new CredentialsManager.CheckCallback() {
                @Override
                public void onSuccess() {
                    credentialsManager.storeCredentials(credentials);
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginView.hideProgress();
                                navigateTo(OverviewFragment.newInstance(), false, true, true);
                            }
                        });
                    }
                }

                @Override
                public void onError(String err) {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginView.hideProgress();
                                Toast.makeText(getActivity(), err, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            };
            credentialsManager.checkCredentials(credentials, getActivity().getApplicationContext(), checkCallback);
        }
    }

    @Override
    public void onSelectCertificate() {
        KeyChain.choosePrivateKeyAlias(getActivity(), alias -> getActivity().runOnUiThread(() -> loginView.showCertAlias(alias)), null, null, null, -1, null);
    }

    @Override
    public void onSendLogs() {
        String log = stringLogger.getLog();

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SMLNotes Logs");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, log);
        startActivity(Intent.createChooser(sharingIntent, "Share Logs"));
    }
}
