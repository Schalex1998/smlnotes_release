package com.smlnotes.smlnotes.screens.notedetail.notedetailview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.databinding.FragmentNoteDetailBinding;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.DefaultEncapsulatedFragmentView;

public class NoteDetailViewImpl extends DefaultEncapsulatedFragmentView<NoteDetailView.NoteDetailViewListener> implements NoteDetailView {

    private FragmentNoteDetailBinding viewBinding;

    public NoteDetailViewImpl(LayoutInflater inflater, ViewGroup container) {
        viewBinding = FragmentNoteDetailBinding.inflate(inflater, container, false);
        setRootView(viewBinding.getRoot());
    }

    @Nullable
    @Override
    public Bundle getViewState() {
        Bundle bundle = new Bundle();
        bundle.putString(VIEW_STATE_CONTENT, viewBinding.etContent.getText().toString());
        return bundle;
    }

    @Override
    public String getTitle() {
        return getContext().getString(R.string.app_name);
    }

    @Override
    public int getOptionsMenu() {
        return R.menu.fragment_note_detail_toolbar;
    }

    @Override
    public void bind(Note note) {
        viewBinding.setNote(note);
    }

    @Override
    public void clearText() {
        viewBinding.etContent.setText("");
    }
}
