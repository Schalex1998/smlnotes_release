package com.smlnotes.smlnotes.screens;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.smlnotes.smlnotes.screens.common.view.EncapsulatedView;
import com.smlnotes.smlnotes.screens.login.loginview.LoginView;
import com.smlnotes.smlnotes.screens.login.loginview.LoginViewImpl;
import com.smlnotes.smlnotes.screens.notedetail.notedetailview.NoteDetailView;
import com.smlnotes.smlnotes.screens.notedetail.notedetailview.NoteDetailViewImpl;
import com.smlnotes.smlnotes.screens.overview.overviewview.OverviewView;
import com.smlnotes.smlnotes.screens.overview.overviewview.OverviewViewImpl;
import com.smlnotes.smlnotes.screens.search.searchview.SearchView;
import com.smlnotes.smlnotes.screens.search.searchview.SearchViewImpl;

public class ViewFactory {

    private final LayoutInflater mLayoutInflater;

    public ViewFactory(LayoutInflater layoutInflater) {
        mLayoutInflater = layoutInflater;
    }

    public <T extends EncapsulatedView> T newInstance(Class<T> viewClass, @Nullable ViewGroup container) {

        EncapsulatedView encapsulatedView;

        if (viewClass == LoginView.class) {
            encapsulatedView = new LoginViewImpl(mLayoutInflater, container);
        } else if (viewClass == OverviewView.class) {
            encapsulatedView = new OverviewViewImpl(mLayoutInflater, container);
        } else if (viewClass == NoteDetailView.class) {
            encapsulatedView = new NoteDetailViewImpl(mLayoutInflater, container);
        } else if (viewClass == SearchView.class) {
            encapsulatedView = new SearchViewImpl(mLayoutInflater, container);
        } else {
            throw new IllegalArgumentException("unsupported view class " + viewClass);
        }

        //noinspection unchecked
        return (T) encapsulatedView;
    }
}
