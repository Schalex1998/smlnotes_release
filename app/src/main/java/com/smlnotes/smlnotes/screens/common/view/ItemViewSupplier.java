package com.smlnotes.smlnotes.screens.common.view;

import android.view.ViewGroup;

public interface ItemViewSupplier<ItemType> {

    ItemView<ItemType> supply(ViewGroup parent);
}
