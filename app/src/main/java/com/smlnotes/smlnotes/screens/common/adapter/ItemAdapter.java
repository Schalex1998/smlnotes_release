package com.smlnotes.smlnotes.screens.common.adapter;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public interface ItemAdapter<ItemType> {

    void setItems(List<ItemType> items);

    RecyclerView.Adapter getAdapter();
}
