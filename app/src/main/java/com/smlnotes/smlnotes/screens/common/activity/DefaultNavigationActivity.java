package com.smlnotes.smlnotes.screens.common.activity;

import android.app.Activity;
import android.content.Intent;

import com.smlnotes.smlnotes.construction.presentation.BaseActivity;

public abstract class DefaultNavigationActivity extends BaseActivity implements NavigationActivity {

    @Override
    public void navigateTo(Class<? extends Activity> clazz, boolean clearBackStack) {
        Intent intent = new Intent(this, clazz);
        if (clearBackStack)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        if (clearBackStack)
            finish();
    }
}
