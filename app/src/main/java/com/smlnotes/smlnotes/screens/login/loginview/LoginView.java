package com.smlnotes.smlnotes.screens.login.loginview;

import com.smlnotes.smlnotes.common.EventEmitter;
import com.smlnotes.smlnotes.model.network.Credentials;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ProgressView;

public interface LoginView extends EncapsulatedFragmentView, ProgressView, EventEmitter<LoginView.LoginViewListener> {

    String VIEW_STATE_METHOD = "view_state_method";
    String VIEW_STATE_USER = "view_state_user";
    String VIEW_STATE_PASSWORD = "view_state_password";
    String VIEW_STATE_URL = "view_state_url";
    String VIEW_STATE_CERT = "view_state_cert";

    void showCertAlias(String alias);

    void bind(Credentials credentials);

    interface LoginViewListener {
        void onLogin();

        void onSelectCertificate();

        void onSendLogs();
    }
}
