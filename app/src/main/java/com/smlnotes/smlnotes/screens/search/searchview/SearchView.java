package com.smlnotes.smlnotes.screens.search.searchview;

import com.smlnotes.smlnotes.common.EventEmitter;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.screens.common.view.EncapsulatedFragmentView;
import com.smlnotes.smlnotes.screens.common.view.ProgressView;

import java.util.List;

public interface SearchView extends EncapsulatedFragmentView, ProgressView, EventEmitter<SearchView.SearchViewListener> {

    void bind(List<Note> notes);

    interface SearchViewListener {
        void onNoteSelected(Note note);

        void onSearch(String searchString);
    }
}
