package com.smlnotes.smlnotes.screens.activity;

import android.os.Bundle;

import com.smlnotes.smlnotes.R;
import com.smlnotes.smlnotes.screens.common.activity.DefaultNavigationActivity;

public class SplashActivity extends DefaultNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        navigateTo(MainActivity.class, true);
    }
}
