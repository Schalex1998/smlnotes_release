package com.smlnotes.smlnotes.common;

import androidx.core.util.Consumer;

import java.util.HashSet;
import java.util.Set;

public abstract class DefaultEventEmitter<ListenerType> implements EventEmitter<ListenerType> {

    private Set<ListenerType> listeners = new HashSet<>();

    @Override
    public void registerListener(ListenerType listener) {
        if (listener != null) listeners.add(listener);
    }

    @Override
    public void unregisterListener(ListenerType listener) {
        listeners.remove(listener);
    }

    protected void forEachListener(Consumer<ListenerType> consumer) {
        for (ListenerType listener : listeners) {
            consumer.accept(listener);
        }
    }
}
