package com.smlnotes.smlnotes.common;

public interface EventEmitter<ListenerType> {
    void registerListener(ListenerType listener);

    void unregisterListener(ListenerType listener);
}
