package com.smlnotes.smlnotes.construction.application;

import com.smlnotes.smlnotes.construction.presentation.PresentationComponent;
import com.smlnotes.smlnotes.construction.presentation.PresentationModule;
import com.smlnotes.smlnotes.construction.service.ServiceComponent;
import com.smlnotes.smlnotes.construction.service.ServiceModule;
import com.smlnotes.smlnotes.screens.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DataModule.class
})
public interface ApplicationComponent {
    PresentationComponent newPresentationComponent(PresentationModule presentationModule);

    ServiceComponent newServiceComponent(ServiceModule serviceModule);

    void inject(SMLnotesApplication smLnotesApplication);
}
