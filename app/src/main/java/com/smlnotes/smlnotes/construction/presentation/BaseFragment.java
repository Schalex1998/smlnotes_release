package com.smlnotes.smlnotes.construction.presentation;

import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;

import com.smlnotes.smlnotes.construction.application.ApplicationComponent;
import com.smlnotes.smlnotes.construction.application.SMLnotesApplication;

import java.util.Objects;

public abstract class BaseFragment extends Fragment {

    private boolean isInjectorUsed;

    private ApplicationComponent getApplicationComponent() {
        return ((SMLnotesApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent();
    }

    @UiThread
    protected PresentationComponent getPresentationComponent() {
        if (isInjectorUsed)
            throw new RuntimeException("only inject once");
        isInjectorUsed = true;
        return getApplicationComponent().newPresentationComponent(new PresentationModule(getActivity()));
    }
}
