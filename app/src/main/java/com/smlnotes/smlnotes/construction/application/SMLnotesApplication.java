package com.smlnotes.smlnotes.construction.application;

import android.app.Application;

import com.smlnotes.smlnotes.model.logging.LoggingConfigurator;
import com.smlnotes.smlnotes.model.logging.StringLogger;
import com.smlnotes.smlnotes.model.network.AutoSyncManager;

import javax.inject.Inject;

public class SMLnotesApplication extends Application {
    @Inject
    AutoSyncManager autoSyncManager;
    @Inject
    LoggingConfigurator loggingConfigurator;

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        getApplicationComponent().inject(this);

        loggingConfigurator.configure();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
