package com.smlnotes.smlnotes.construction.presentation;

import androidx.lifecycle.ViewModel;

import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.screens.common.view.ViewModelFactory;
import com.smlnotes.smlnotes.screens.notedetail.NoteDetailViewModel;
import com.smlnotes.smlnotes.screens.overview.OverviewViewModel;
import com.smlnotes.smlnotes.screens.search.SearchViewModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @Provides
    ViewModelFactory provideViewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(OverviewViewModel.class)
    ViewModel provideOverviewViewModel(NotesRepository notesRepository) {
        return new OverviewViewModel(notesRepository);
    }

    @Provides
    @IntoMap
    @ViewModelKey(NoteDetailViewModel.class)
    ViewModel provideNoteDetailViewModel(NotesRepository notesRepository) {
        return new NoteDetailViewModel(notesRepository);
    }

    @Provides
    @IntoMap
    @ViewModelKey(SearchViewModel.class)
    ViewModel provideSearchViewModel(NotesRepository notesRepository) {
        return new SearchViewModel(notesRepository);
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }
}
