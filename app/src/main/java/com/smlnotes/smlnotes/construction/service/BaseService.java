package com.smlnotes.smlnotes.construction.service;

import androidx.annotation.UiThread;
import androidx.core.app.JobIntentService;

import com.smlnotes.smlnotes.construction.application.ApplicationComponent;
import com.smlnotes.smlnotes.construction.application.SMLnotesApplication;

public abstract class BaseService extends JobIntentService {

    private boolean isInjectorUsed;

    private ApplicationComponent getApplicationComponent() {
        return ((SMLnotesApplication) getApplication()).getApplicationComponent();
    }

    @UiThread
    protected ServiceComponent getServiceComponent() {
        if (isInjectorUsed)
            throw new RuntimeException("only inject once");
        isInjectorUsed = true;
        return getApplicationComponent().newServiceComponent(new ServiceModule(this));
    }
}
