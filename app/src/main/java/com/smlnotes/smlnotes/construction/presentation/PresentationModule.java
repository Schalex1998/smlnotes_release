package com.smlnotes.smlnotes.construction.presentation;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.smlnotes.smlnotes.common.DialogUtil;
import com.smlnotes.smlnotes.screens.ViewFactory;
import com.smlnotes.smlnotes.screens.common.dialog.DialogManager;

import dagger.Module;
import dagger.Provides;

@Module
public class PresentationModule {

    private final FragmentActivity activity;

    PresentationModule(FragmentActivity activity) {
        this.activity = activity;
    }

    @Provides
    LayoutInflater provideLayoutInflater() {
        return LayoutInflater.from(activity);
    }

    @Provides
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides
    DialogUtil provideDialogUtil(Context context) {
        return new DialogUtil(context);
    }

    @Provides
    DialogManager provideDialogManager(FragmentManager fragmentManager) {
        return new DialogManager(fragmentManager);
    }

    @Provides
    Context provideContext() {
        return activity;
    }

    @Provides
    ViewFactory provideViewFactory(LayoutInflater layoutInflater) {
        return new ViewFactory(layoutInflater);
    }
}
