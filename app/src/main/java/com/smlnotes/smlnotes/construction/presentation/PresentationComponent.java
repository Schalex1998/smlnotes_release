package com.smlnotes.smlnotes.construction.presentation;

import com.smlnotes.smlnotes.screens.activity.MainActivity;
import com.smlnotes.smlnotes.screens.login.LoginFragment;
import com.smlnotes.smlnotes.screens.notedetail.NoteDetailFragment;
import com.smlnotes.smlnotes.screens.overview.OverviewFragment;
import com.smlnotes.smlnotes.screens.search.SearchFragment;
import com.smlnotes.smlnotes.screens.settings.PreferenceFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PresentationModule.class,
        ViewModelModule.class
})
public interface PresentationComponent {

    void inject(LoginFragment loginFragment);

    void inject(OverviewFragment overviewFragment);

    void inject(NoteDetailFragment noteDetailFragment);

    void inject(SearchFragment searchFragment);

    void inject(MainActivity mainActivity);

    void inject(PreferenceFragment preferenceFragment);
}
