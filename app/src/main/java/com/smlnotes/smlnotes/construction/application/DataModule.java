package com.smlnotes.smlnotes.construction.application;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.room.Room;

import com.smlnotes.lib.ConnectionConfig;
import com.smlnotes.lib.SMLClient;
import com.smlnotes.smlnotes.model.logging.LoggingConfigurator;
import com.smlnotes.smlnotes.model.logging.StringLogger;
import com.smlnotes.smlnotes.common.AppExecutors;
import com.smlnotes.smlnotes.model.NotesRepository;
import com.smlnotes.smlnotes.model.database.Note;
import com.smlnotes.smlnotes.model.database.NoteDao;
import com.smlnotes.smlnotes.model.database.NoteDatabase;
import com.smlnotes.smlnotes.model.network.AutoSyncManager;
import com.smlnotes.smlnotes.model.network.ConnectionConfigBuilder;
import com.smlnotes.smlnotes.model.network.Credentials;
import com.smlnotes.smlnotes.model.network.CredentialsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class DataModule {

    @Provides
    NoteDatabase provideNoteDatabase(Application application) {
        return Room.databaseBuilder(application, NoteDatabase.class, "note_database").build();
    }

    @Provides
    NoteDao provideNoteDao(NoteDatabase noteDatabase) {
        return noteDatabase.noteDao();
    }

    @Singleton
    @Provides
    NotesRepository provideNotesRepository(Application application, NoteDao noteDao, AppExecutors appExecutors, SharedPreferences sharedPreferences) {
        return new NotesRepository(application, noteDao, appExecutors, sharedPreferences);
    }

    @Singleton()
    @Provides()
    AutoSyncManager provideAutoSyncManager(NotesRepository notesRepository, SharedPreferences sharedPreferences, AppExecutors appExecutors, CredentialsManager credentialsManager) {
        AutoSyncManager autoSyncManager = new AutoSyncManager(notesRepository, sharedPreferences, appExecutors, credentialsManager);
        autoSyncManager.init();
        return autoSyncManager;
    }

    @Singleton()
    @Provides()
    LoggingConfigurator provideLoggingConfigurator(StringLogger stringLogger) {
        return new LoggingConfigurator(stringLogger);
    }

    @Singleton()
    @Provides()
    StringLogger provideStringLogger() {
        return new StringLogger();
    }

    @Singleton
    @Provides
    ConnectionConfigBuilder provideConnectionConfigBuilder() {
        return new ConnectionConfigBuilder();
    }

    @Provides
    CredentialsManager provideCredentialsManager(SharedPreferences sharedPreferences, SMLClient smlClient, ConnectionConfigBuilder connectionConfigBuilder, Application application) {
        CredentialsManager credentialsManager = new CredentialsManager(sharedPreferences, smlClient, connectionConfigBuilder);

        if (credentialsManager.isLoggedIn()) {
            Credentials credentials = credentialsManager.getCredentials();
            ConnectionConfig connectionConfig = connectionConfigBuilder.buildConnectionConfig(credentials, application.getApplicationContext());
            smlClient.reconfigure(connectionConfig);
        }

        return credentialsManager;
    }

    @Singleton
    @Provides
    SMLClient provideSMLClient(NotesRepository notesRepository) {
        ConnectionConfig connectionConfig = ConnectionConfig.builder().build();
        return new SMLClient<Note>(notesRepository, connectionConfig);
    }
}
