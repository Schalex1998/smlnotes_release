package com.smlnotes.smlnotes.construction.presentation;

import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;

import com.smlnotes.smlnotes.construction.application.ApplicationComponent;
import com.smlnotes.smlnotes.construction.application.SMLnotesApplication;

public abstract class BaseActivity extends AppCompatActivity {

    private boolean isInjectorUsed;

    private ApplicationComponent getApplicationComponent() {
        return ((SMLnotesApplication) getApplication()).getApplicationComponent();
    }

    @UiThread
    protected PresentationComponent getPresentationComponent() {
        if (isInjectorUsed)
            throw new RuntimeException("only inject once");
        isInjectorUsed = true;
        return getApplicationComponent().newPresentationComponent(new PresentationModule(this));
    }
}
