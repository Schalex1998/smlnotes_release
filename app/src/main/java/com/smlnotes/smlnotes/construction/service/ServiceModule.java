package com.smlnotes.smlnotes.construction.service;

import android.app.Service;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    private final Service service;

    ServiceModule(Service service) {
        this.service = service;
    }

    @Provides
    Context provideContext() {
        return service;
    }
}
