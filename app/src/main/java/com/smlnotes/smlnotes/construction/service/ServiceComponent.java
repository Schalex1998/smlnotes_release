package com.smlnotes.smlnotes.construction.service;

import com.smlnotes.smlnotes.model.network.SyncService;

import dagger.Subcomponent;

@Subcomponent(modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService syncService);
}
